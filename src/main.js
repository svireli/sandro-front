import Vue from 'vue';
import Buefy from 'buefy';
import VueResource from 'vue-resource';
import VueCookie from 'vue-cookie';
import 'buefy/dist/buefy.css';
import '@mdi/font/css/materialdesignicons.min.css';
import App from './App.vue';
import router from './router';

Vue.use(VueResource);
Vue.use(VueCookie);
Vue.use(Buefy);
Vue.config.productionTip = false;

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', Vue.cookie.get('bauth-token'));
  next();
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
